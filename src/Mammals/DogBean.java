package Mammals;

/**
 * Created by daltonsolo on 9/14/2017.
 */

// DogBean inherits from MammalBean

public class DogBean extends MammalBean {

    private String breed;
    private String name;

    // Constructor function

    public DogBean(int legCount, String color, double height, String breed, String name) {
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    // Getter and Setter methods

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // toString


    @Override
    public String toString() {
        return "legCount: " + getLegCount() + "\ncolor: " + getColor() + "\nheight: " + getHeight() + "\nbreed: " + breed + "\nname: " + name;
    }
}
