package Mammals;

/**
 * Created by daltonsolo on 9/16/2017.
 */
public class Kennel {
    public static void main(String[] args) {
        Kennel kennel = new Kennel();
        DogBean[] allDogs = kennel.buildDogs();
        kennel.displayDogs(allDogs);
    }
    private DogBean[] buildDogs() {
        DogBean dog1 = new DogBean(4, "brown", 10.5, "Husky", "Dogmeat");
        DogBean dog2 = new DogBean(3, "white", 10.5, "Chiwawa", "Chico");
        DogBean dog3 = new DogBean(4, "tan", 10.5, "Golden Retriever", "Keeper");
        DogBean dog4 = new DogBean(4, "black", 10.5, "Pitbull", "Rocky");
        DogBean dog5 = new DogBean(4, "black", 10.5, "Doberman Pinscher", "Charlie");

        DogBean[] dogArray = {dog1, dog2, dog3, dog4, dog5};
        return dogArray;
    }

    private void displayDogs(DogBean[] dogArray) {
        for (int i=0; i<dogArray.length; i++) {
            System.out.println(dogArray[i]);
        }
    }


}
