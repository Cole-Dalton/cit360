package Collections;
import java.util.*;
import java.io.*;
public class Main {
    public static void main(String[] args) {
        Map<String, Account> treeMapVar = new TreeMap<String, Account>();
        File file = new File("C:/Users/daltonsolo/Documents/CIT 360 - Fall 2017/cit360/src/Collections/rawData.csv");
        BufferedReader br = null;
        String line;
        try {
            br = new BufferedReader(new FileReader(file));

            while ((line = br.readLine()) != null) {
                String[] csv = line.split(",");
                if(!treeMapVar.containsKey(csv[0])) {
                    Account acc = new Account(csv[0], csv[1], csv[2], csv[3]);
                    treeMapVar.put(csv[0],acc);
                }
            }
            for (Map.Entry<String, Account> entry : treeMapVar.entrySet()){
                System.out.println(entry.getValue().toString());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}